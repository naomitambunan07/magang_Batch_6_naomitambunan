# Nama : Naomi Tambunan
# Posisi : Magang Sistem Analis
# Batch 6

CREATE DATABASE javan

Use javan;

select * from javan.consumercomplaints;

/Jumlah komplain setiap bulan/
SELECT MONTH(`Date Received`) AS MONTH, COUNT(`Complaint ID`) AS TOTAL_COMPLAINT FROM javan.consumercomplaints
GROUP BY MONTH(`Date Received`)

# /Komplain yang memiliki tags ‘Older American’/
SELECT * FROM javan.consumercomplaints
WHERE Tags = 'Older American';

#Buat sebuah view yang menampilkan data nama perusahaan, jumlah company response to consumer seperti tabel di bawah

create view consumercomplaintsv as
select Company,
       count(case when 'Company Response to Consumer' = 'Closed' then 1 else 0 end) Closed,
       count(case when 'Company Response to Consumer' = 'Closed with explanation' then 1 else 0 end) 'Closed with explanation',
       count(case when 'Company Response to Consumer' = 'Closed with non-monetary relief' then 1 else 0 end) 'Closed with non-monetary relief'
from consumercomplaints c
group by Company;

select *
from consumercomplaintsv;


